## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1sSmFhrfB6fFuIgF9QNLJJR-SUz2pzx4Z/preview"></iframe>

## Mergesort and count inversion

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1qomBYgm-UX92AjJ5eAFkp89pFcFVuaPt/preview"></iframe>

## Practice Problem

1. Count occurrence
    - run the binary search to find where the target number appears at first, let's say that at index i
    - run the binary search to find where the target number appears at last, let's say that at index j 
    - return i - j

2. Find the extra element
3. Majority Element

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1p2Mr2SkxMsS5DL7sxzrdP89o_S1m1rgS/preview"></iframe>

## Solution

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1u-sY6pGywrU6Dvwbk5ciinHbNCihummY/preview"></iframe>

## Solution

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1OHyOV4Hyo4H9kP-9U9xtRCqUNCnCra1D/preview"></iframe>

## Do It Yourself

1. Count Less Sensitive Inversions

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1syPi5YG950bAlbNvowpqANqYqRNr6rVC/preview"></iframe>

## Solution

1. Count Less Sensitive Inversions

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/11h3ciwHV3_NSC7cylg6WFYMGlqgdqArc/preview"></iframe>

## Solution

1. Count Less Sensitive Inversions

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/18_Fon2On0aa4orwWoPpnQLan-QGz99x4/preview"></iframe>





































