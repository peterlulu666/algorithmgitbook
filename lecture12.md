## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1VymFEVXt4ijxHHlYtkSqvv9WH3M0KH-i/preview"></iframe>

## Unbounded Knapsack

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1SwNhdgMeI986Qe-CDertAcBcT27G2KcA/preview"></iframe>

## Python Code

```python
# Unbounded Knapsack
def knapsack_repetition(item_value, item_weight, number_of_item, capacity):
    dp = [-1] * (number_of_item + 1)
    for c in range(1, capacity + 1):
        for n in range(0, number_of_item):
            if (c >= item_weight[n]):
                dp[c] = max(dp[c], dp[c - item_weight[n]] + item_value[n])
    return dp[capacity]

item_value = {0: 1, 1: 2, 2: 3}
item_weight = {0: 1, 1: 2, 2: 3}
print(knapsack_repetition(item_value, item_weight, 3, 2))

```

## Do It Yourself

1. the minimum steps to one
    1. subtract 1 from it
    2. if it is divisible by 2, divide by 2
    3. if it is divisible by 3, divide by 3

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1dd7XFf7D21vgepNahkm0fcpmK_xF9sop/preview"></iframe>

## Solution

1. the minimum steps to one
    1. subtract 1 from it
    2. if it is divisible by 2, divide by 2
    3. if it is divisible by 3, divide by 3

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1jsJfIayg_hMLLdLZC9ZA2zlXTC8DeLUy/preview"></iframe>

## Solution

1. the minimum steps to one
    1. subtract 1 from it
    2. if it is divisible by 2, divide by 2
    3. if it is divisible by 3, divide by 3

```java
public class MinSteps {
    public static void main(String[] args) {
        System.out.println(MinSteps(100));
    }

    static int MinSteps(int n) {
        int table[] = new int[n + 1];
        for (int i = 0; i <= n; i++) table[i] = n - i;
        for (int i = n; i >= 1; i--) {
            if (i >= 1)
                table[i - 1] = Math.min(table[i] + 1, table[i - 1]);
            if (i % 2 == 0)
                table[i / 2] = Math.min(table[i] + 1, table[i / 2]);
            if (i % 3 == 0)
                table[i / 3] = Math.min(table[i] + 1, table[i / 3]);
        }
        return table[1];
    }

```

```python
def minToOne(number):
    memoized = [-1] * (number + 1)
    for n in range(0, number + 1):
        memoized[n] = number - n
    for n in range(number, 0, -1):
        if n >= 1:
            memoized[int(n - 1)] = min(memoized[n] + 1, memoized[int(n - 1)])
        if n % 2 == 0:
            memoized[int(n / 2)] = min(memoized[n] + 1, memoized[int(n / 2)])
        if n % 3 == 0:
            memoized[int(n / 3)] = min(memoized[n] + 1, memoized[int(n / 3)])
    return memoized[1]

```

## exercise

1. Coin change
    1. if greater than 25, subtract 25 from it
    2. if greater than 10, subtract 10 from it
    3. if greater than 5, subtract 5 from it
    4. if greater than 1, subtract 1 from it

```python
def coinChange(number):
    memoized = [-1] * (number + 1)
    for n in range(0, number + 1):
        memoized[n] = number - n
    for n in range(number, 0, -1):
        if n >= 25:
            memoized[int(n - 25)] = min(memoized[n] + 1, memoized[int(n - 25)])
        if n >= 10:
            memoized[int(n - 10)] = min(memoized[n] + 1, memoized[int(n - 10)])
        if n >= 5:
            memoized[int(n - 5)] = min(memoized[n] + 1, memoized[int(n - 5)])
        if n >= 1:
            memoized[int(n - 1)] = min(memoized[n] + 1, memoized[int(n - 1)])
    return memoized[0]

```






