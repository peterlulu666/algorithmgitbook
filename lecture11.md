## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1EMF-Oe0rtOt9s9NWA-rpN4C54QQsBh5Z/preview"></iframe>

## Python Code

```python
def fib(n):
    if n == 1 or n == 2:
        result = 1
    else:
        result = fib(n - 1) + fib(n - 2)
    return result


def fibMemoized(n, memoized):
    if n in memoized:
        return memoized[n]
    if n == 1 or n == 2:
        result = 1
    else:
        result = fibMemoized(n - 1, memoized) + fibMemoized(n - 2, memoized)
    memoized[n] = result
    return result


def fib_botton_up(n):
    if n == 1 or n == 2:
        return 1
    fib = {}
    # base case
    fib[1] = 1
    fib[2] = 1
    # generic case
    for i in range(3, n + 1):
        fib[i] = fib[i - 1] + fib[i - 2]
    return fib[n]


print(fib(10))

memoized = {}
print(fibMemoized(10, memoized))

print(fib_botton_up(10))

```

## Do It Yourself

1. counter example
2. run time
3. overlapping subproblems

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1PDkU6pUsJ4MdYDwzRua5LjZak5kewGNw/preview"></iframe>

## Solution

1. counter example
2. run time
3. overlapping subproblems

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1jVtx91WImIBpVoCToShETKn-uX-7ODk3/preview""></iframe>

## Solution

1. counter example
2. run time
3. overlapping subproblems 

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/159nfrmxoVSCacGdAe5_WZrR7aja0DND3/preview"></iframe>

