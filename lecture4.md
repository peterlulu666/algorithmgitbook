## Lecture    

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1Bv2-gs382HZeJIH3P7838IiNnjYuEvij/preview"></iframe>

## Do It Yourself

1. Duplicated 

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1NsAzM3dr-WFyuakbmE0MAos0dLdRi2EL/preview"></iframe>

## Solution

1. Duplicated

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1doGVRQf6VhOLUOsgxF5kP3O1u5J_995P/preview"></iframe>

## Solution

1. Duplicated
2. Non duplicated

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1jT-luemswsEMQ2vEd_WKyEDBgmQxGJkd/preview"></iframe>

## Homework

1. T(n)
2. Divide and Conquer Run Time
3. Shifted array largest number in array
4. Fixed Point

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1tfMBgFbXohOrTmgXxBiAcyX3Hup9gG-k/preview"></iframe>

## Solution

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/12HcirK_YkUQMGoOcVQTEWbWy3qCi6-DV/preview"></iframe>

## Solution

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1qyKHCkMIwUdcTEhEVryCSbgMtdQqSQDF/preview"></iframe>

5.Shifted array position of input number

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/16UzkXEnO9XN4vX9zAyiHdf9Tfk2CRypr/preview"></iframe>

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1KrdgLBuwT7s85mm_lE1gIz4I73mtPzV9/preview"></iframe>


## Quiz 3

1. Peak Time

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1VZIjKY28K7iDMqyhDYGepoLGd3SlSI5k/preview"></iframe>

## Solution

1. Peak Time

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1dd7Gj47aVgY1HOBOT-nnxIBfcjIpK3l-/preview"></iframe>


<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/13UO-2BYXMxmePwZMd1xFlUm0KgNXcSFj/preview"></iframe>



## Exercise

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1BcXdyWGdQpVp4ay-dJIgPejSpKVuUlfH/preview"></iframe>

## Solution

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1XtnpIgm4fOk_I_40PzxDUlS11nIOM0rl/preview"></iframe>

















