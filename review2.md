## Review 2

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1auHA8ij0s1YVApPVfF-aQ4RDnG__qiog/preview"></iframe>

## Solution

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1Y1RIFR10H4rVKoNJ3QqneULzz0ZRmsMi/preview"></iframe>

## Solution

1.Kruskal connected component

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1hUpjU9UVv8XxudWyswfxYgeshU7AD8o1/preview"></iframe>

2.Dijkstra negative edge weight longest path

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1uIKGS489x-dXHeQE4DkJRhiBn5PxIK33/preview"></iframe>

3.Dynamic Programming

1. Coin Change
2. Unbounded Knapsack
3. Longest Rising Trend

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1Se3oexQsb7biosW1ekVGkNil3W3uDdv5/preview"></iframe>

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1YcAc66fsk9o1dZLsMNWT7bEJ12EKzUyP/preview"></iframe>

## Exercise

1. The Knapsack Problem

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1ngTvmkyo3TpzR-VPt8j9Y8rZviR9547y/preview"></iframe>









