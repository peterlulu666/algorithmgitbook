## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1HlqdI3-I8Wg3_0S_0MTJ58hs4FCW2pcd/preview"></iframe>

## Breadth First Search

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/11R0CT5PITL6QyRX75fqyi11O8aeaDw-C/preview"></iframe>

## Python Code

```python
graph_dictionary = {"A": ["B", "C"],
                    "B": ["D", "E", "C"],
                    "C": ["A", "B", "F"],
                    "D": ["B"],
                    "E": ["B", "F"],
                    "F": ["E", "C"]}


def main():
    global graph_dictionary

    BFS("A", graph_dictionary)


def BFS(selecting_node, graph_dictionary):
    queue = []
    visited_list = []
    # add last
    queue.append(selecting_node)
    # add last
    visited_list.append(selecting_node)

    while len(queue) != 0:
        # remove first
        dequeue_node = queue.pop(0)

        adjacent_node_list = graph_dictionary[dequeue_node]

        for node in adjacent_node_list:
            if node not in visited_list:
                # add last
                queue.append(node)
                # add last
                visited_list.append(node)

        print(dequeue_node)


main()

```

## Shortest Path

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1ekIjp4HDuksptA5qNSceFoFzXobzx94m/preview"></iframe>

## Python Code

```python
graph_dictionary = {"A": ["B", "C"],
                    "B": ["D", "E", "C"],
                    "C": ["A", "B", "F"],
                    "D": ["B"],
                    "E": ["B", "F"],
                    "F": ["E", "C"]}


def main():
    global graph_dictionary
    shortest_path("A", graph_dictionary)


def shortest_path(selecting_node, graph_dictionary):
    queue = []
    visited_list = []
    # add last
    queue.append(selecting_node)
    # add last
    visited_list.append(selecting_node)
    shortest_path = {}
    shortest_path[selecting_node] = 0

    while len(queue) != 0:
        # remove first
        dequeue_node = queue.pop(0)

        adjacent_node_list = graph_dictionary[dequeue_node]

        for node in adjacent_node_list:
            if node not in visited_list:
                # add last
                queue.append(node)
                # add last
                visited_list.append(node)
                # shortest path of the node is the shortest path of the parent node + 1
                shortest_path[node] = shortest_path[dequeue_node] + 1

    for key in graph_dictionary.keys():
        print(str(key) + ": " + str(shortest_path[key]))


main()

```

## BFS Bipartite Shortest Path

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1wTWtCwJ9zjyrVVkwfGtbfTR8ME3qd0GZ/preview"></iframe>

## Python Code

```python
graph_dictionary = {"A": ["B", "C"],
                    "B": ["D", "E"],
                    "C": ["A", "F"],
                    "D": ["B"],
                    "E": ["B"],
                    "F": ["C"]}

color = {}
for key in graph_dictionary.keys():
    color[key] = "white"

shortest_path = {}


def main():
    global graph_dictionary

    print(BFS("A", graph_dictionary))

    for key in graph_dictionary.keys():
        print(str(key) + ": " + str(shortest_path[key]))


def BFS(selecting_node, graph_dictionary):
    global color
    global shortest_path

    queue = []

    # add last
    queue.append(selecting_node)

    color[selecting_node] = "grey"

    shortest_path[selecting_node] = 0

    while len(queue) != 0:
        # remove first
        dequeue_node = queue.pop(0)

        adjacent_node_list = graph_dictionary[dequeue_node]

        for node in adjacent_node_list:
            if color[node] == "white":
                # add last
                queue.append(node)

                color[node] = "grey"

                shortest_path[node] = shortest_path[dequeue_node] + 1

            elif color[node] == color[dequeue_node]:
                return False

        color[dequeue_node] = "black"
        print(dequeue_node)

    return True


main()

```

## Do It Yourself

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1NJ8jGIZMb9hGc8rBwjiQ29U6EYSMSAEc/preview"></iframe>

## Solution

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/15tya6JmrN_ccn7B_kyDOrhmRuvEtypdv/preview"></iframe>

## Homework

1. Shortest path
2. weighted shortest path
3. Making team using bipartite

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/10meDpUQfKbsp7CP2qen6ZRzkAF2PsWtq/preview"></iframe>

## Solution

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1S59Zc7kmlkbJWXpuuOOOrC3n3qGmAj_h/preview"></iframe>

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1OrnNDMdP_lffpZNdHg0EmSddUWWYMbtj/preview"></iframe>

## Quiz 4

1. fish tank BFS

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1kk22mtUSZQBl1RcgwPTYkvsoZQp-MIDo/preview"></iframe>

## Exercise

1. Check 2 color using bipartite 
2. Check odd cycle graph using bipartite

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1iNAUDNQGF7PBEjkSc8xG7B8sp_G5_AqN/preview"></iframe>

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1KtmFBmQQw3BhneJq0V8Db3JcGZFVTTsH/preview"></iframe>

## Solution

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1TdkDNTM30WiDjZWqjjUK229GUj6z_Mt7/preview"></iframe>





















