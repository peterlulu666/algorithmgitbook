# Summary

* [Introduction](README.md)
* [Lecture 1 Introduction](lecture1.md)
* [Lecture 2 Algorithm Analysis](lecture2.md)
* [Lecture 3 Gale-Shapley Algorithm](lecture3.md)
* [Lecture 4 Divide and Conquer Binary Search](lecture4.md)
* [Lecture 5 Divide and Conquer MergeSort](lecture5.md)
* [Lecture 6 BFS](lecture6.md)
* [Lecture 7 DFS](lecture7.md)
* [Lecture 8 Dijkstras](lecture8.md)
* [Lecture 9 Minimum Spanning Trees](lecture9.md)
* [Lecture 10 Greedy Algorithms Activity](lecture10.md)
* [Lecture 11 Introduction to dynamic programming](lecture11.md)
* [Lecture 12 Knapsacks](lecture12.md)
* [Lecture 13 Knapsacks 01](lecture13.md)
* [Lecture 14 Bellman Ford](lecture14.md)
* [Lecture 15 Max Flow](lecture15.md)
* [Lecture 16 Max Flow Examples](lecture16.md)
* [Lecture 17 NP Complete](lecture17.md)
* [Lecture 18 NP Reductions](lecture18.md)
* [Lecture 19 NP Reductions](lecture19.md)
* [Exam](exam.md)
    * [Review 1](review1.md)
    * [Exam 1](exam1.md)
    * [Review 2](review2.md)
    * [Review 3](review3.md)









