## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1EZaOxO6ZAOnLprZ6f2V0KoG2K83hPc7J/preview"></iframe>

## Depth First Search Topological Order

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1EbWUuuFt49jgF4srZsKjHZ32IYZmatF1/preview"></iframe>

## Python Code 


```python
graph_dictionary = {"A": ["B", "C"],
                    "B": ["D", "E", "C"],
                    "C": ["A", "B", "F"],
                    "D": ["B"],
                    "E": ["B", "F"],
                    "F": ["E", "C"]}

# Initialize
color_dictionary = {}
for key in graph_dictionary.keys():
    color_dictionary[key] = "white"

parent_dictionary = {}
for key in graph_dictionary.keys():
    parent_dictionary[key] = None

# count the timestamps
time = 0

discovery_time_dictionary = {}
for key in graph_dictionary.keys():
    discovery_time_dictionary[key] = -1

finishing_time_dictionary = {}
for key in graph_dictionary.keys():
    finishing_time_dictionary[key] = -1

DFS_print = []
topological_order_print = []


def main():
    DFS("A")

    print(DFS_print)
    print(topological_order_print)

    for key in graph_dictionary.keys():
        print(str(key) + ": " +
              str(discovery_time_dictionary[key]) +
              ", " +
              str(finishing_time_dictionary[key]))


def DFS(selecting_node):
    global color_dictionary
    global time
    global discovery_time_dictionary
    global graph_dictionary
    global parent_dictionary
    global finishing_time_dictionary
    global DFS_print
    global topological_order_print

    color_dictionary[selecting_node] = "grey"
    DFS_print.append(selecting_node)
    time = time + 1
    discovery_time_dictionary[selecting_node] = time

    adjacent_nodes_list = graph_dictionary[selecting_node]

    for node in adjacent_nodes_list:
        if color_dictionary[node] == "white":
            parent_dictionary[node] = selecting_node
            DFS(node)

    color_dictionary[selecting_node] = "black"
    topological_order_print.append(selecting_node)
    time = time + 1
    finishing_time_dictionary[selecting_node] = time


main()

```

## Python Code

```python
graph_dictionary = {"A": ["B", "C"],
                    "B": ["D", "E", "C"],
                    "C": ["A", "B", "F"],
                    "D": ["B"],
                    "E": ["B", "F"],
                    "F": ["E", "C"]}


def main():
    global graph_dictionary
    DFS_Stack("A", graph_dictionary)


def DFS_Stack(selecting_node, graph_dictionary):
    stack = []
    visited_list = []
    # add first
    stack.insert(0, selecting_node)
    # add last
    visited_list.append(selecting_node)

    while len(stack) != 0:
        # remove first
        popped_node = stack.pop(0)

        adjacent_nodes_list = graph_dictionary[popped_node]

        for node in adjacent_nodes_list:
            if node not in visited_list:
                # add fist
                stack.insert(0, node)
                # add last
                visited_list.append(node)
        print(popped_node)


main()

```

## Practice Problem

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/16PgAlnPMjGU55fnUMt6VDP7G38s2eRj7/preview"></iframe>

## Solution

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1sKr9sExjeDGjFg4WEdX45P5iZ0vdsDdL/preview"></iframe>

## Do It Yourself

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/17QJp7b7vlDMivjcuUQb6U9eR04zr2Sw2/preview"></iframe>

## Solution

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1usgNKWQMf1SmQe2hz1ykMb6RbnAwMam6/preview"></iframe>








