## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1T7jC6mbkN3a-Eln4o3SRSC2qo59-RcDE/preview"></iframe>

## Dijkstra

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1yJUtvGvvIdehmz7c1yORGEksWrDA2moz/preview"></iframe>

## Python Code

```python
graph = {"a": {"b": 10, "c": 3},
         "b": {"c": 1, "d": 2},
         "c": {"b": 4, "d": 8, "e": 2},
         "d": {"e": 7},
         "e": {"d": 9}}
infinity = float("inf")


def dijkstra(selecting_node):
    shortest_distance = {}
    unvisited = graph
    # let all nodes' distance to be infinity
    for node in unvisited:
        shortest_distance[node] = infinity
    # let the selecting node's distance to be 0
    shortest_distance[selecting_node] = 0

    while unvisited:
        # find the node with the smallest distance
        min_node = None
        for node in unvisited:
            # in the first iteration, we will randomly select one node to be the min_node
            # then we will compare it with the other node to find the node with the smallest distance
            if min_node is None:
                min_node = node
            elif shortest_distance[node] < shortest_distance[min_node]:
                min_node = node

        # compare min_node + weight with child_node's distance
        # if shortest_distance[min_node] + weight is smaller, update child_node's distance
        adjacent_node_dict = unvisited[min_node]
        for child_node, weight in adjacent_node_dict.items():
            if shortest_distance[min_node] + weight < shortest_distance[child_node]:
                shortest_distance[child_node] = shortest_distance[min_node] + weight

        # pop the min_node
        unvisited.pop(min_node)

    print(shortest_distance)


dijkstra("a")

```

## Python Code 

1. FIBONACCI HEAP

```python
import heapq

graph = {"a": {"b": 10, "c": 3},
         "b": {"a": 10, "c": 1, "d": 2},
         "c": {"a": 3, "b": 1, "d": 8, "e": 2},
         "d": {"b": 2, "c": 8, "e": 7},
         "e": {"d": 7, "c": 2}}
infinity = float("inf")


def Dijkstra(selecting_node):
    shortest_distance = {}
    # let all nodes' distance to be infinity
    for node in graph:
        shortest_distance[node] = infinity
    # let the selecting node's distance to be 0
    shortest_distance[selecting_node] = 0

    # Put tuple pair into the priority queue
    unvisited_queue = [(shortest_distance[node], node) for node in graph]
    heapq.heapify(unvisited_queue)

    # store visited node
    visited = []

    # let parent to be null
    parent = {}
    for node in graph:
        parent[node] = None

    # while there exists node in the unvisited_queue
    while len(unvisited_queue):
        # Pops the vertex with the smallest distance
        # (0, 'a')
        min_node_pair = heapq.heappop(unvisited_queue)
        # a
        min_node = min_node_pair[1]

        # append visited node to visited
        visited.append(min_node)

        # update shortest_distance
        # update parent
        adjacent_node_dict = graph[min_node]
        for child_node, weight in adjacent_node_dict.items():
            if child_node not in visited:
                if shortest_distance[min_node] + weight < shortest_distance[child_node]:
                    shortest_distance[child_node] = shortest_distance[min_node] + weight
                    parent[child_node] = min_node

        # Pop every item
        while len(unvisited_queue):
            heapq.heappop(unvisited_queue)

        # Put all vertices not visited into the queue
        unvisited_queue = [(shortest_distance[node], node) for node in graph if node not in visited]
        heapq.heapify(unvisited_queue)

    print(parent)
    print(shortest_distance)


Dijkstra("a")

```



## Do It Yourself  

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1ppJbi6HVd-r08_nLLKk-EXyD2yV4vbww/preview"></iframe>

## Solution

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1sy43cD9cZrPExhK_8KyMWPGPXgcR4QX_/preview"></iframe>

## Homework

1. negative edge weights, double the weight
2. Coloring edge with Dijkstras
3. Multi-Source Shortest Path

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1wIA7lUX7eEFjIqXffsQUI7ayg99uHHDE/preview"></iframe>


## Solution

1. negative edge weights, double the weight
2. Coloring edge with Dijkstras
3. Multi-Source Shortest Path

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1wEZi42M3gelthSvBypuRpctjQqblB9cy/preview"></iframe>

## Exercise

1. Dijkstra traffic
<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1EOkFaLk2X9RGdy6civJqrqCxmlfgvp6A/preview"></iframe>








