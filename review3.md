## Review 3

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1m_lz3Cds8bSbqKQqhxL9dMkYEj0QVRVi/preview"></iframe>

## Solution

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/13c94S-Kdb6orrGhlW3pdXzn0t8Lg_B3I/preview"></iframe>

## Solution

- Package and spaceship
- pseudopolymial

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1CRVT92LC_N_U7Klib-DZh3ZSBOJf8Bo4/preview"></iframe>

- Network flow

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1MmkRafYXTMAfsSACwc9XmmwUBYE6Ev-A/preview"></iframe>

m packages

n trucks

truck capacity is p

We will build the network flow. The package is the vertex, ui, and is on the left side.
The trucks is the vertex, wj, and is on the right side. Connect the package vertex
and the trucks vertex when package goes into the trucks. The edge is directed
and the capacity is 1.      
Construct source vertex, s. Connect the source vertex to the package vertex and the
edge capacity is 1. Construct the sink vertex, t. Connect the trucks to the sink
vertex and the edge capacity is p.      
Run the Ford Fulkerson algorithm to find the maximum flow and minimum cut. If the
maximum flow is m, then every package will connect to the trucks. So the trucks will not be overloaded.      



- Vertex Cover NP-Complete

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1KGgme4n3rS9jOST1aLg_KVojv7PdTK8j/preview"></iframe>

- A ≤P B
    1. B is at least as hard as A
    2. polynomial time reducible to.”
    3. one can provide a
    polynomial time algorithm that transforms any input of A into some input of B such that
    the input to A is a true instance of A if and only if the transformed input to B is a true
    instance of B








<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/154UteZWI5ScmCogA7D-YwhdvuWa4HXYt/preview"></iframe>

- Decision problem
    1. In the decision problem, we consider if the list contains any conflicting people.      
    2. In the decision problem, we consider if every flight has at least one of the captains.  

- Prove in NP
    1. BoatingParty is a decision problem. A certificate of a correct instance of BoatingParty is just the selection of people to bring along. The length of the list is the input size. We would be able to verify the certificate in polynomial time. We can verify the certificate in polynomial time by checking that the set of people does not contain any conflicts. 
    2. Safe Trips is a decision problem. A certificate of a correct instance of Safe Trips is just the selection of list of k captain. The length of the list is the input size. We would be able to verify the certificate in polynomial time by checking that every flight has at least one of the captains.      

- 3 coloring NP-complete

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1haKUCg_jsojZEF1NyJ41UVsfq-XH_m3b/preview"></iframe>

- Clique NP-complete

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1U7c09_JVut6ZgDCGG48vTRvybieUEl5A/preview""></iframe>

- Independent set NP-complete

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1aFSC--wgAJNpl1j63Qkk-01IMrt9vZqe/preview"></iframe>












































