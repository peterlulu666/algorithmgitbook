## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1YkG0thtF09JVX48dgjGAT3RjZTagYNLn/preview"></iframe>

## Do It Yourself

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1BuApaluN9D6QsjBwEow65W2xKlHrDDyY/preview"></iframe>

## Solution

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1eLmn6ZYwao6sTdNyqfXjr5JOxodKmJdh/preview"></iframe>

## Homework

1. Maximum Flow

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1o44VLutMgoTTJYuH85BMMyU9gNqGW0j3/preview"></iframe>

## Solution

1. Maximum Flow

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/14WzRjyivFGE_w1-swYpV_HXE5hiP8RY1/preview"></iframe>

## Solution

1. Maximum Flow

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1Ycf960jwWY41NG3BMh-fTLEo3fX0OScc/preview"></iframe>

## Quiz 9

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/14ySifGHobGLschY1nWrGZwk4npQdIALg/preview"></iframe>

## Exercise

1. Flow
2. Residual Graph
3. Maximum flow
4. minimum cut

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1sA-93XFZT7jvsbX0-337Yx7axt760FX8/preview"></iframe>
















