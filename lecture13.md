## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1t9mf7sOfImQGHcG47njZfvC2SOwEVkIE/preview"></iframe>

## Knapsack Coin Change

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/138n2z9O7JZj9VbnRUbeR0NDu11lPpw--/preview"></iframe>

## Pyhon Code

1.Knapsack

```python
# 0 1 knapsack
def knapsack(value_list, weight_list, number_of_item, capacity):
    # declare 2 D array
    # capacity is the column
    # number_of_item is the row
    dp = [[-1] * (capacity + 1) for x in range(number_of_item + 1)]

    # initialize the 1st row to be 0
    for i in range(0, capacity + 1):
        dp[0][i] = 0

    # initialize the 1st column to be 0
    for i in range(0, number_of_item + 1):
        dp[i][0] = 0

    for n in range(1, number_of_item + 1):
        for c in range(1, capacity + 1):
            # weight_list[0] is at the second row of dp
            # value_list[0] is at the second row of dp
            # the 1 st item in the weight_list is at index 0
            # the 1 st item in the value_list is at index 0
            # they are at the second row of dp
            if c >= weight_list[n - 1]:
                dp[n][c] = max(dp[n - 1][c], dp[n - 1][c - weight_list[n - 1]] + value_list[n - 1])
            else:
                dp[n][c] = dp[n - 1][c]
    return dp[number_of_item][capacity]


value_list = [10, 40, 30, 50]
weight_list = [5, 4, 6, 3]
number_of_item = len(weight_list)
capacity = 6
print(knapsack(value_list, weight_list, number_of_item, capacity))


# Unbounded Knapsack
def knapsack_repetition(item_value, item_weight, number_of_item, capacity):
    dp = [-1] * (number_of_item + 1)
    for c in range(1, capacity + 1):
        for n in range(0, number_of_item):
            if (c >= item_weight[n]):
                dp[c] = max(dp[c], dp[c - item_weight[n]] + item_value[n])
    return dp[capacity]

```

2.Fewest Coin Change

```python
# 1 D array coin change
def fewest_coin_change(denomination, number_of_denomination, money):
    dp = [-1] * (money + 1)
    dp[0] = 0

    for m in range(1, money + 1):
        fewest = float("inf")
        for d in range(1, number_of_denomination):
            if m >= denomination[d]:
                if 1 + dp[m - denomination[d]] < fewest:
                    fewest = 1 + dp[m - denomination[d]]
            dp[m] = fewest
    return dp[money]


denomination = [1, 5, 6, 8]
number_of_denomination = len(denomination)
money = 11
print(fewest_coin_change(denomination, number_of_denomination, money))


# 2 D array coin change
def coin_change(denomination, number_of_denomination, money):
    # declare 2 D array
    # money is the column
    # denomination is the row
    dp = [[0] * (money + 1) for x in range(number_of_denomination)]

    # initialize the 1st column to be 0
    for i in range(0, number_of_denomination):
        dp[i][0] = 0
    # initialize the 1st row to be 0 - money
    for i in range(0, money + 1):
        dp[0][i] = i

    for d in range(1, number_of_denomination):
        for m in range(1, money + 1):
            if m >= denomination[d]:
                dp[d][m] = min(dp[d - 1][m], dp[d][m - denomination[d]] + 1)
            else:
                dp[d][m] = dp[d - 1][m]

    return dp[number_of_denomination - 1][money]


denomination = [1, 5, 6, 8]
number_of_denomination = len(denomination)
money = 11
print(coin_change(denomination, number_of_denomination, money))

```

3.Ways of Coin Change

```python
# if denomination contains 1
def ways_coin_change(denomination_list, number_of_denomination, money):
    # declare 2 D array
    # money is the column
    # denomination is the row
    dp = [[0] * (money + 1) for x in range(number_of_denomination)]
    # initialize the 1st row
    for m in range(0, money + 1):
        if m < denomination_list[0]:
            dp[0][m] = 0
        else:
            if m % denomination_list[0] == 0:
                dp[0][m] = 1
            else:
                dp[0][m] = 0
    # initial the 1st column to be 1
    for d in range(0, number_of_denomination):
        dp[d][0] = 1

    for d in range(1, number_of_denomination):
        for m in range(1, money + 1):
            # denomination_list is starting at the first row of the dp
            # so it is the denomination_list[d]
            if m >= denomination_list[d]:
                dp[d][m] = dp[d - 1][m] + dp[d][m - denomination_list[d]]
            else:
                dp[d][m] = dp[d - 1][m]

    return dp[number_of_denomination - 1][money]


denomination_list = [1, 2]
number_of_denomination = len(denomination_list)
money = 5
print(ways_coin_change(denomination_list, number_of_denomination, money))


# if denomination does not contains 1 
def ways_of_coin_change(denomination_list, number_of_denomination, money):
    # declare 2 D array
    # money is the column
    # denomination is the row
    dp = [[0] * (money + 1) for x in range(number_of_denomination + 1)]
    # initial the 1st column to be 1
    for d in range(0, number_of_denomination + 1):
        dp[d][0] = 1

    for d in range(1, number_of_denomination + 1):
        for m in range(1, money + 1):
            # denomination_list is starting at the second row of the dp
            # so it is the denomination_list[d - 1]
            if m >= denomination_list[d - 1]:
                dp[d][m] = dp[d - 1][m] + dp[d][m - denomination_list[d - 1]]
            else:
                dp[d][m] = dp[d - 1][m]

    return dp[number_of_denomination][money]


denomination_list = [1, 2]
number_of_denomination = len(denomination_list)
money = 5
print(ways_of_coin_change(denomination_list, number_of_denomination, money))

```

## Do It Yourself

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/10TPa0D3mpGhPfAOZ5xT7mnCBO2LUpLdr/preview"></iframe>

## Solution

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1P9UYAOLc52ETFfQCn0YKWHgPZw6af1_Z/preview"></iframe>

## Exercise

1. Knapsack
2. Greedy Algorithm Sorted Weight Sorted Value

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/16YvCT3mvgtmwWyWbJ9U5qvnRiUqkPiVJ/preview"></iframe>














