## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1MtjVWzZIUbGAC8yxhkwSL0kdjzv0RLeK/preview"></iframe>

## Do It Yourself

1. Greedy Algorithm songs on CD

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1ac73OMJtkTQEIOaM16mAu2-iDvew19c5/preview"></iframe>

## Solution

1. Greedy Algorithm songs on CD

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1FaQapkybGFT3g53cLyTPLvMsjhX0YGFp/preview"></iframe>

## Homework

1. Greedy Choice Justification
2. Phone Base Stations
    - line
    - circle
3. Thievery
    - Thief steal house that is not adjacent

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1Df1Ie8_0iXDozlTmlFWVaqt5JdD9IT50/preview"></iframe>

## Solution

1. truck load the greedy algorithm stays ahead
2. Phone Base Stations the greedy algorithm stays ahead
    - line
    - circle
3. Thievery

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1jD0x7_rwCVrtFNi-EMS-Bm7I6DALsZfV/preview"></iframe>

## Solution


1.truck load the greedy algorithm stays ahead

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1sxEswcw5w4Ntfm8oTSfGcGwqx6E9SyNk/preview"></iframe>

2.Phone Base Stations the greedy algorithm stays ahead

1. line
2. circle

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1hd4-Vse7c-u2w7EUhDZggUsOErw7u434/preview"></iframe>

## Quiz 7

1. trash bin the greedy algorithm stays ahead 

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/16d7HPhCr4X5Y465RewA7y6G1lNb1NTUt/preview"></iframe>

## Exercise 

1. knapsack greedy algorithm
    - sort the item value in descending order
    - Keep including each item until the weight limit is reached









