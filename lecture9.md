## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1qbXindDrrNalw2EiwqhmyO7sZuV3whWq/preview"></iframe>

## Prim

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1R7gdcme6dL1-zh9kM_ZSSRj6FjZrdHeQ/preview"></iframe>

## Python Code

```python
graph = {"a": {"b": 10, "c": 3},
         "b": {"a": 10, "c": 1, "d": 2},
         "c": {"a": 3, "b": 1, "d": 8, "e": 2},
         "d": {"b": 2, "c": 8, "e": 7},
         "e": {"d": 7, "c": 2}}
infinity = float("inf")


def prim(selecting_node):
    # store the minimum edge weight in keys
    keys = {}
    # store the parent node in parent
    parent = {}
    # store graph to unvisited
    unvisited = graph
    # let key to be infinity
    for node in unvisited:
        keys[node] = infinity
    # let parent to be null
    for node in unvisited:
        parent[node] = None
    # let the selecting node's key to be 0
    keys[selecting_node] = 0

    # while there exists node in the unvisited
    while unvisited:
        # find the node with the smallest key
        compare_key = {}
        for node in unvisited:
            compare_key[node] = keys[node]
        min_node = min(compare_key, key=compare_key.get)

        # update parent and keys
        adjacent_node_dict = unvisited[min_node]
        for child_node, weight in adjacent_node_dict.items():
            if (child_node in unvisited) and (weight < keys[child_node]):
                parent[child_node] = min_node
                keys[child_node] = weight

        # pop the min_node
        unvisited.pop(min_node)

    # print the edge weight
    print(keys)
    # print the tree
    for key, value in parent.items():
        print(str(value) + " -- " + str(key))


prim("a")

```

## Implement Prim's algorithm with a Fibonacci heap

<iframe 
height="842"
width="100%"
src="src/prim.html"></iframe>

## Python Code

```python
import heapq

graph = {"a": {"b": 10, "c": 3},
         "b": {"a": 10, "c": 1, "d": 2},
         "c": {"a": 3, "b": 1, "d": 8, "e": 2},
         "d": {"b": 2, "c": 8, "e": 7},
         "e": {"d": 7, "c": 2}}
infinity = float("inf")


def prim(selecting_node):
    keys = {}
    # let all nodes' keys to be infinity
    for node in graph:
        keys[node] = infinity
    # let the selecting node's key to be 0
    keys[selecting_node] = 0

    # Put tuple pair into the priority queue
    unvisited_queue = [(keys[node], node) for node in graph]
    heapq.heapify(unvisited_queue)

    # store visited node
    visited = []

    # let parent to be null
    parent = {}
    for node in graph:
        parent[node] = None

    # while there exists node in the unvisited_queue
    while len(unvisited_queue):
        # Pops a vertex with the smallest key
        # (0, 'a')
        min_node_pair = heapq.heappop(unvisited_queue)
        # a
        min_node = min_node_pair[1]

        # append visited node to visited
        visited.append(min_node)

        # update keys
        # update parent
        adjacent_node_dict = graph[min_node]
        for child_node, weight in adjacent_node_dict.items():
            if (child_node not in visited) and (weight < keys[child_node]):
                parent[child_node] = min_node
                keys[child_node] = weight

        # pop every item
        while len(unvisited_queue):
            heapq.heappop(unvisited_queue)

        # put all vertices not visited into the queue
        unvisited_queue = [(keys[node], node) for node in graph if node not in visited]
        heapq.heapify(unvisited_queue)

    # print the edge weight
    print(keys)
    # print the tree
    for key, value in parent.items():
        print(str(value) + " -- " + str(key) + " edge weight is " + str(keys[key]))


prim("a")

```

## Practice Problem

1. Greedy Ski Distribution
2. Coin Changing
    - find the largest coin from coin_list
    - update count_coin[largest coin]
    - subtract largest coin from coin
3. Scheduling a Triathlon

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1ALBw31zku0w-vDkn98iUf_E0akcRC1k_/preview"></iframe>

## Solution

1. Greedy Ski Distribution
2. Coin Changing
    - find the largest coin from coin_list
    - update count_coin[largest coin]
    - subtract largest coin from coin
3. Scheduling a Triathlon

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1hnweVtntTY__jflzUNaltpAVAaFoxiw0/preview"></iframe>

## Solution

1. Greedy Ski Distribution
2. Coin Changing
    - find the largest coin from coin_list
    - update count_coin[largest coin]
    - subtract largest coin from coin
3. Scheduling a Triathlon
 
<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1ThY5JoGDSon3K5TI8vgToGesqvwCHQ2h/preview"></iframe>

## Do It Yourself

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1Z1flNICSeIox-o2GCW47Ld7S8oNLnvvy/preview"></iframe>

## Solution

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1aBM5o1tnYvnbmkPBgDk7OShLQlTHHxoI/preview"></iframe>

## Solution

1. edge weight increased by 1
2. edge weight squared

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1JBMxArPMGcQPmZqYC6L_rezEvxVkBeLU/preview"></iframe>

## Homework

1. Shortest Paths in minimum spanning tree and shortest Paths in full grah are different
2. Bottleneck Edges in Minimum Spanning Trees
3. Phone Base Stations the greedy algorithm stays ahead the greedy algorithm an exchange argument
    - line
    - circle

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1lY7yhvP8DlcD_ZPoJTu_VOSY7yXsvfDB/preview"></iframe>

## Solution

1. Shortest Paths in minimum spanning tree and shortest Paths in full grah are different
2. Bottleneck Edges in Minimum Spanning Trees
3. Phone Base Stations the greedy algorithm stays ahead the greedy algorithm an exchange argument
    - line
    - circle

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1GtHr5R0BtNyRrQhZOeQ-RucNM41ld_AY/preview"></iframe>

## Solution

1.Shortest Paths in minimum spanning tree and shortest Paths in full grah are different

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/11n92nUPcz-Se7FgS1Rb_O-RPB8yKMFeX/preview"></iframe>

3.Phone Base Stations the greedy algorithm stays ahead the greedy algorithm an exchange argument

1. line
2. circle
    
<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1b-nj9i0-EAA3_aRuJnsCdTE-H2gPJ0M8/preview"></iframe>

## Quiz 5 and 6

1. Dijkstra
    1. design
    2. run time
    3. proof
2. edge weight squared

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1uWXU_0Yuoq6D45QMaOscbK48GD2otPxV/preview"></iframe>

## Exercise

1.Petrol station the greedy algorithm stays ahead

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1sYavn4ujBN6qHWINxNdHIPlsa3t4oTdN/preview"></iframe>

2.Trash bin the greedy algorithm stays ahead

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1yLjI09OZAjIc6QOinb92Nt57Nts8eKwI/preview"></iframe>








