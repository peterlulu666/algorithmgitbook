## Lecture

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1qvOTg2CwrxUSONLeyT0vVct4RUoRkZZJ/preview"></iframe>

## Practice Problem

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1Cf1LW_VpWX0UBs4NJgsL6b6YMkxFidkz/preview"></iframe>

## Solution

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1MeJeMCq-eIzubSwmaU4rb-nC_gVgzkTq/preview"></iframe>

## Do It Yourself

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1XbcUdqkCzr73IygVHCVFmp8cPxbeE8wT/preview"></iframe>

## Solution

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1_A7-J6iBO_5qammCAJM3-HEfxHY3MpZW/preview"></iframe>

## Homework

2.Equal Sum Partition

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1gENodHp1SXCzBu88wbcMCAUNLY0z3Saa/preview"></iframe>

## Solution

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1XUqjKmPqweYYNftbXhZrFTZqeaz7r35B/preview"></iframe>

## Exercise

1. Longest Common Subsequence

<iframe 
height="842"
width="100%"
src="https://drive.google.com/file/d/1yEa5FB7z_1L7ar9Z6j2SxEz8rFQ_lBPk/preview"></iframe>












